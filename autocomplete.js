// initially adding EventHandler
document.getElementById('username').addEventListener('input', onInputChange);

async function onInputChange() {
  let searchString = document.getElementById('username').value;
  const parts = searchString.split(' ');
  document.getElementById('username').value = parts[parts.length - 1];

  const usernameDatalist = document.getElementById('usernames');
  usernameDatalist.innerHTML = ''; // remove all datalist options

  if (!checkUsernameRequirements(searchString) || searchString.length <= 2) return;

  const autocompletionList = await getAutocompletionList(searchString);
  if (!autocompletionList) {
    console.log('Invalid Search Term')
    return;
  }

  usernameDatalist.innerHTML = ''; // remove all datalist options

  for (let suggestion of autocompletionList.result) { // append options (suggestions)
    let option = document.createElement('option');

    option.value = suggestion.online ? '🟢 ' : '⚫️ ';
    option.value += suggestion.title ? `${suggestion.title} ` : '';
    option.value += suggestion.name;

    usernameDatalist.appendChild(option);
  }
}

async function getAutocompletionList(pSearchString) {
  try {
    const response = await fetch(`https://lichess.org/api/player/autocomplete?term=${encodeURIComponent(pSearchString)}&object=true`);
    const jsonData = await response.json();
    if (jsonData.error) { return null }
    return jsonData;
  } catch (error) {
    console.error('There has been a problem with your fetch operation:', error);
  }
}

function checkUsernameRequirements(pUsername) {
  const regex = /^(?!.*[-_]{2})[a-zA-Z0-9][a-zA-Z0-9_-]{0,28}[a-zA-Z0-9]$/; 
  return regex.test(pUsername);
}