import { visualizeUserData, visualizeGamesPlayed, visualizeWorldMaps, visualizePieceMoves, visualizeResultsByOpponentsRating } from './visualizeData.js';

export async function getUsernamesPublicData(pUsername) {
  if (!checkUsernameRequirements(pUsername)) {
    return null;
  }

  try {
    const response = await fetch(`https://lichess.org/api/user/${encodeURIComponent(pUsername)}`);
    const jsonData = await response.json();
    if (jsonData.error) { return null }
    return jsonData;
  } catch (error) {
    console.error('There has been a problem with your fetch operation:', error);
  }
}

function checkUsernameRequirements(pUsername) {
  const regex = /^(?!.*[-_]{2})[a-zA-Z0-9][a-zA-Z0-9_-]{0,28}[a-zA-Z0-9]$/;
  return regex.test(pUsername);
}

export async function getGamesOfUser(pUsername, pInterval, pRated, pTimeClass) {

  let url = new URL(`https://lichess.org/api/games/user/${encodeURIComponent(pUsername)}`);

  let startDateTimestamp;
  let now = new Date();
  switch (pInterval) {
    case '1day':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1).getTime();
      break;
    case '7days':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7).getTime();
      break;
    case '1month':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate()).getTime();
      break;
    case '3month':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth() - 3, now.getDate()).getTime();
      break;
    case '1year':
      startDateTimestamp = new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()).getTime();
      break;
    default:
      startDateTimestamp = 0;
      break;
  }
  url.searchParams.set('since', startDateTimestamp);
  if (pRated !== 'null') {
    url.searchParams.set('rated', pRated);
  }
  if (pTimeClass !== 'null') {
    url.searchParams.set('perfType', pTimeClass);
  }

  let aborter = new AbortController();

  let abortButton = document.getElementById('abortButton');
  abortButton.addEventListener('click', () => {
    aborter.abort();
    // todo: visualize download aborted
    console.log('Download aborted');
  }, { once: true });

  let gamesFoundSpan = document.getElementById('gamesFound');
  gamesFoundSpan.innerText = 0;

  try {
    const response = await fetch(url, {
      headers: {
        'Accept': 'application/x-ndjson'
      },
    });

    let userGames = [];

    let reader = response.body.getReader();
    let decoder = new TextDecoder();
    let partial = '';

    while (true) {
      const { done, value } = await reader.read();

      if (aborter.signal.aborted) {
        reader.cancel();
        break;
      }

      if (done) {
        break;
      }

      partial += decoder.decode(value, { stream: true });

      const lines = partial.split(/\r?\n/);
      partial = lines.pop();

      const json = '[' + lines.join(',').replace(/,\s*$/, '') + ']';
      const jsondata = JSON.parse(json);

      for (let game of jsondata) {
        userGames.push(game);
        gamesFoundSpan.innerText = userGames.length;
      }
    }

    if (userGames.length == 0) return null;

    return userGames;

  } catch (error) {
    console.error(`Download error: ${error.message}`);
  }
}

export async function getOpponentsPublicData(pUserGames, pUserID) {
  // create set of opponents
  let opponentsSet = new Set();
  for (let game of pUserGames) {
    if (game.players.black.aiLevel || game.players.white.aiLevel) continue; // if opponent is stockfish
    if (game.players.black?.user?.id) { // if opponent is anonymous
      opponentsSet.add(game.players.black.user.id);
    }
    if (game.players.white?.user?.id) { // if opponent is anonymous
      opponentsSet.add(game.players.white.user.id);
    }
  }
  opponentsSet.delete(pUserID);

  // split up Set into Arrays with max. 300 entries - here 200

  let opponentsArr = Array.from(opponentsSet.values())
  const opponentsArrChunks = [];
  const chunkSize = 200;
  for (let i = 0; i < opponentsArr.length; i += chunkSize) {
    opponentsArrChunks.push(opponentsArr.slice(i, i + chunkSize));
  }

  return await fetchOpponentsPublicData(opponentsArrChunks, 0, pUserID, pUserGames);
}

async function fetchOpponentsPublicData(pOpponentsArrChunks, pStartingIndexForChunks, pUserID, pUserGames, pOpponentsDataObj) {
  // fetch opponents public data (max 300 users per fetch - here 200, max 8000 users per 10 minutes)
  let opponentsDataObj;
  if(pOpponentsDataObj) {
    opponentsDataObj = pOpponentsDataObj
  } else {
    opponentsDataObj = {};
  }
  let i = pStartingIndexForChunks;
  try {
    while (pOpponentsArrChunks[i] && i < pStartingIndexForChunks + 40) { // 40 = 8000 / 200)
      const response = await fetch(`https://lichess.org/api/users`, {
        method: 'POST',
        headers: {
          'Content-Type': 'text/plain'
        },
        body: Array.from(pOpponentsArrChunks[i]).join(',')
      });
      if (!response.ok) {
        throw new Error("Network response was not OK");
      }
      const jsonData = await response.json();
      if (jsonData.error) { return null }

      // add user to opponentsDataObj
      jsonData.forEach(user => {
        opponentsDataObj[user.id] = user
      });

      i++;
    }
  } catch (error) {
    console.error('There has been a problem with your fetch operation:', error);
    document.getElementById('error').innerText = 'Connection Issue: Unable to establish a stable connection. Please check your network settings.';
    document.getElementById('error').style.display = 'block';
  }

  if (pOpponentsArrChunks[i]) { // if still chunks left
    document.getElementById('error').innerText = 'Fetching Opponents Data will resume in 10 minutes due to API limitations. Diagrams might be incomplete. \n Please avoid making additional requests during this time.';
    document.getElementById('error').style.display = 'block';
    window.opponentsPublicDataID = setTimeout(async () => { // make recursive call
      let opponentsDataObj2 = await fetchOpponentsPublicData(pOpponentsArrChunks, i, pUserID, pUserGames, opponentsDataObj);
      if (!opponentsDataObj2) {
        document.getElementById('error').innerText = 'Connection Issue: Unable to establish a stable connection. Please check your network settings.';
        document.getElementById('error').style.display = 'block';
        return;
      }

      // reset statistics
      document.getElementById('svgMapAmountOpponents').innerHTML = '';
      document.getElementById('svgMapAmountGames').innerHTML = '';
      document.getElementById('svgMapWinLossRatio').innerHTML = '';

      // update statistics
      let countriesPopulation = await getCountriesPopulation();

      visualizeWorldMaps(pUserGames, pUserID, opponentsDataObj2, countriesPopulation);
    }, 10 * 60 * 1000); // wait 10 minutes
  } else {
    document.getElementById('error').style.display = 'none';
  }

  return opponentsDataObj;
}

export async function getCountriesPopulation() {
  try {
    const response = await fetch(`https://restcountries.com/v3.1/all?fields=cca2,population`);
    const jsonData = await response.json();
    if (jsonData.error) { return null; }

    const countriesPopulation = {};
    jsonData.forEach(item => {
      countriesPopulation[item.cca2] = item.population;
    });
    return countriesPopulation;
  } catch (error) {
    console.error('There has been a problem with your fetch operation:', error);
  }
}