import { getUsernamesPublicData, getGamesOfUser, getOpponentsPublicData, getCountriesPopulation } from './fetchData.js';
import { visualizeUserData, visualizeGamesPlayed, visualizeWorldMaps, visualizePieceMoves, visualizeResultsByOpponentsRating } from './visualizeData.js';

// initially adding EventHandler
document.getElementById('submitButton').addEventListener('click', onSubmitButtonPress);
document.getElementById('toggleDarkmodeButton').addEventListener('click', toggleDarkMode);

async function onSubmitButtonPress() { // main
  let errorAlert = document.getElementById('error');
  errorAlert.style.display = 'none';

  // store fetch settings
  let interval = document.getElementById('interval').value;
  let rated = document.getElementById('rated').value;
  let timeClass = document.getElementById('timeClass').value;

  // get public data of entered user
  let usernameInput = document.getElementById('username').value;

  let userData = await getUsernamesPublicData(usernameInput);
  if (!userData) {
    errorAlert.innerText = 'Invalid Username or Connection Issues: The provided username is invalid, or there are connection problems. \nPlease verify your input and try again.';
    errorAlert.style.display = 'block';
    return;
  }
  let username = userData.username;

  // visualize user data
  document.getElementsByClassName('result-container')[0].style.display = 'block';
  visualizeUserData(userData);

  // reset statistics (todo: loop over array of charts)
  Chart.getChart('gamesPlayedChart')?.destroy();
  document.getElementById('gamesPlayedChart').style.display = 'none';
  document.getElementById('gamesPlayedStripChart').style.display = 'none';
  document.getElementById('svgMapAmountOpponents').innerHTML = '';
  document.getElementById('svgMapAmountGames').innerHTML = '';
  document.getElementById('svgMapWinLossRatio').innerHTML = '';
  Chart.getChart('piecesMovesChart')?.destroy();
  document.getElementById('piecesMovesChart').style.display = 'none';
  Chart.getChart('resultsRatingChart')?.destroy();
  document.getElementById('resultsRatingChart').style.display = 'none';

  // fetch user games
  let userGames = await getGamesOfUser(username, interval, rated, timeClass);
  if (!userGames) {
    errorAlert.innerText = 'No Games Found: Please check your network settings or adjust your filter settings';
    errorAlert.style.display = 'block';
    return;
  }

  // fetch opponents public data
  if (window.opponentsPublicDataID) { // abort recursive call of fetchOpponentsPublicData
    clearTimeout(window.opponentsPublicDataID);
  }
  window.opponentsPublicDataID = undefined; // global variable

  let opponentsDataObj = await getOpponentsPublicData(userGames, userData.id);
  if (!opponentsDataObj) {
    errorAlert.innerText = 'Connection Issue: Unable to establish a stable connection. Please check your network settings.';
    errorAlert.style.display = 'block';
    return;
  }

  let countriesPopulation = await getCountriesPopulation();

  /* user statistics */
  document.getElementById('gamesPlayedChart').style.display = 'inline';
  visualizeGamesPlayed(userGames, interval, userData.id);

  document.getElementById('gamesPlayedStripChart').style.display = 'flex';

  visualizeWorldMaps(userGames, userData.id, opponentsDataObj, countriesPopulation);

  document.getElementById('piecesMovesChart').style.display = 'inline';
  visualizePieceMoves(userGames, userData.id);

  document.getElementById('resultsRatingChart').style.display = 'inline';
  visualizeResultsByOpponentsRating(userGames, userData.id);
}

let isDarkMode = false;

if(window?.matchMedia('(prefers-color-scheme: dark)')?.matches) { // browser settings for light/dark-mode
  toggleDarkMode();
}

function toggleDarkMode() {
  const body = document.querySelector('body');
  const container = document.querySelector('.container');
  const darkModeToggle = document.getElementById('toggleDarkmodeButton');

  isDarkMode = !isDarkMode;

  if (isDarkMode) {
    body.classList.add('dark-mode');
    container.classList.add('dark-mode2');
    darkModeToggle.textContent = 'Light Mode';
  } else {
    body.classList.remove('dark-mode');
    container.classList.remove('dark-mode2');
    darkModeToggle.textContent = 'Dark Mode';
  }
}