import { countryNamesEN } from './countriesEN.js';

export function visualizeUserData(pUserData) {
  let FIDETitles = {
    'GM': 'Grandmaster',
    'IM': 'International Master',
    'FM': 'FIDE Master',
    'NM': 'National Master',
    'CM': 'Candidate Master',
    'WGM': 'Woman Grandmaster',
    'WIM': 'Woman International Master',
    'WFM': 'Woman FIDE Master',
    'WNM': 'Woman National Master',
    'WCM': 'Woman Candidate Master',
    'LM': 'Lichess Master',
  }

  document.getElementById('userCountry').src = pUserData?.profile?.country && pUserData.profile.country[0] != '_' ? `https://cdn.jsdelivr.net/gh/hjnilsson/country-flags@latest/svg/${pUserData.profile.country.toLowerCase()}.svg` : '';
  document.getElementById('userCountry').title = pUserData?.profile?.country && pUserData.profile.country[0] != '_' ? countryNamesEN[pUserData.profile.country] : '';
  document.getElementById('userCountry').alt = pUserData?.profile?.country && pUserData.profile.country[0] != '_' ? countryNamesEN[pUserData.profile.country] : '';
  document.getElementById('userTitle').innerText = pUserData.title ? pUserData.title : '';
  document.getElementById('userTitle').title = pUserData.title ? FIDETitles[pUserData.title] : '';
  document.getElementById('userName').innerText = pUserData.username;
  document.getElementById('userName').href = pUserData.url;
  document.getElementById('userFideRating').innerText = pUserData?.profile?.fideRating ? pUserData.profile.fideRating : '';
  document.getElementById('userFirstName').innerText = pUserData?.profile?.firstName ? pUserData.profile.firstName : '';
  document.getElementById('userLastName').innerText = pUserData?.profile?.lastName ? pUserData.profile.lastName : '';
  document.getElementById('userPatron').innerText = pUserData.patron ? '' : '';
}

export function visualizeGamesPlayed(pUserGames, pInterval, pUserID) {

  let monthNamesEN = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  let data = {};
  let now = new Date();
  let startDateTimestamp;

  switch (pInterval) {
    case '1day':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1).getTime();
      // create labels
      for (let i = startDateTimestamp; i <= now; i += 1000 * 60 * 60) {
        data[new Date(i).toLocaleString()] = { wins: 0, losses: 0, draws: 0 };
      }
      // count games for each label
      for (let game of pUserGames) {
        if (game?.status == 'draw') { // draw
          data[new Date(new Date(game.createdAt).setMinutes(0, 0, 0)).toLocaleString()].draws++;
        } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
          data[new Date(new Date(game.createdAt).setMinutes(0, 0, 0)).toLocaleString()].wins++;
        } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
          data[new Date(new Date(game.createdAt).setMinutes(0, 0, 0)).toLocaleString()].wins++;
        } else { // loss
          data[new Date(new Date(game.createdAt).setMinutes(0, 0, 0)).toLocaleString()].losses++;
        }
      }
      break;

    case '7days':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7).getTime();
      for (let i = startDateTimestamp; i <= now; i += 1000 * 60 * 60 * 24) {
        data[new Date(i).toLocaleDateString()] = { wins: 0, losses: 0, draws: 0 };
      }
      for (let game of pUserGames) {
        if (game?.status == 'draw') { // draw
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].draws++;
        } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].wins++;
        } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].wins++;
        } else { // loss
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].losses++;
        }
      }
      break;

    case '1month':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate()).getTime();
      for (let i = startDateTimestamp; i <= now; i += 1000 * 60 * 60 * 24) {
        data[new Date(i).toLocaleDateString()] = { wins: 0, losses: 0, draws: 0 };
      }
      for (let game of pUserGames) {
        if (game?.status == 'draw') { // draw
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].draws++;
        } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].wins++;
        } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].wins++;
        } else { // loss
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].losses++;
        }
      }
      break;

    case '3month':
      startDateTimestamp = new Date(now.getFullYear(), now.getMonth() - 3, now.getDate()).getTime();
      for (let i = startDateTimestamp; i <= now; i += 1000 * 60 * 60 * 24) {
        data[new Date(i).toLocaleDateString()] = { wins: 0, losses: 0, draws: 0 };
      }
      for (let game of pUserGames) {
        if (game?.status == 'draw') { // draw
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].draws++;
        } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].wins++;
        } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].wins++;
        } else { // loss
          data[new Date(new Date(game.createdAt).setHours(0, 0, 0, 0)).toLocaleDateString()].losses++;
        }
      }
      break;

    case '1year':
      startDateTimestamp = new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()).getTime();
      for (let i = startDateTimestamp; i <= now; i = new Date(new Date(new Date(new Date(new Date(new Date(i).setDate(1)).setMonth(new Date(i).getMonth() + 1)).setHours(0)).setMinutes(0)).setSeconds(0)).setMilliseconds(0)) {
        data[new Date(i).getFullYear() + ', ' + monthNamesEN[new Date(i).getMonth()]] = { wins: 0, losses: 0, draws: 0 };
      }
      for (let game of pUserGames) {
        if (game?.status == 'draw') { // draw
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].draws++;
        } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].wins++;
        } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].wins++;
        } else { // loss
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].losses++;
        }
      }
      break;

    default: // all
      startDateTimestamp = pUserGames[pUserGames.length - 1].createdAt;
      for (let i = startDateTimestamp; i <= now; i = new Date(new Date(new Date(new Date(new Date(new Date(i).setDate(1)).setMonth(new Date(i).getMonth() + 1)).setHours(0)).setMinutes(0)).setSeconds(0)).setMilliseconds(0)) {
        data[new Date(i).getFullYear() + ', ' + monthNamesEN[new Date(i).getMonth()]] = { wins: 0, losses: 0, draws: 0 };
      }
      for (let game of pUserGames) {
        if (game?.status == 'draw') { // draw
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].draws++;
        } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].wins++;
        } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].wins++;
        } else { // loss
          data[new Date(game.createdAt).getFullYear() + ', ' + monthNamesEN[new Date(game.createdAt).getMonth()]].losses++;
        }
      }
      break;
  };

  new Chart(document.getElementById('gamesPlayedChart'), {
    type: 'bar',
    options: {
      plugins: {
        legend: {
          display: true
        },
        tooltip: {
          backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-background'),
          titleColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-black'),
          bodyColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-gray'),
          mode: 'index',
        }
      },
      scales: {
        x: {
          stacked: true,
        },
        y: {
          stacked: true
        }
      }
    },
    data: {
      labels: Object.keys(data),
      datasets: [{
        label: 'Losses',
        data: Object.values(data).map(item => item.losses),
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-loss-color'),
        borderColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-loss-border-color'),
        borderWidth: '1',
        grouped: true,
      }, {
        label: 'Draws',
        data: Object.values(data).map(item => item.draws),
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-draw-color'),
        borderColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-draw-border-color'),
        borderWidth: '1',
        grouped: true,
      }, {
        label: 'Wins',
        data: Object.values(data).map(item => item.wins),
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-win-color'),
        borderColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-win-border-color'),
        borderWidth: '1',
        grouped: true,
      }]
    }
  });

  // Strip-Chart

  let winsBar = document.getElementById('stripChartWins');
  let drawsBar = document.getElementById('stripChartDraws');
  let lossesBar = document.getElementById('stripChartLosses');
  let results = {
    wins: 0,
    draws: 0,
    losses: 0
  };
  for (let game of pUserGames) { // second iteration for improved readability
    if (game?.status == 'draw') { // draw
      results.draws++;
    } else if (game.players.white?.user?.id == pUserID && game?.winner == 'white') { // win
      results.wins++;
    } else if (game.players.black?.user?.id == pUserID && game?.winner == 'black') { // win
      results.wins++;
    } else { // loss
      results.losses++;
    }
  }

  if (results.wins > 0) { winsBar.style.borderWidth = '1px'; winsBar.style.overflow = 'visible'; } else { winsBar.style.borderWidth = '0px'; winsBar.style.overflow = 'hidden'; };
  if (results.draws > 0) { drawsBar.style.borderWidth = '1px'; drawsBar.style.overflow = 'visible'; } else { drawsBar.style.borderWidth = '0px'; drawsBar.style.overflow = 'hidden'; };
  if (results.losses > 0) { lossesBar.style.borderWidth = '1px'; lossesBar.style.overflow = 'visible'; } else { lossesBar.style.borderWidth = '0px'; lossesBar.style.overflow = 'hidden'; };

  let gamesTotal = pUserGames.length;

  winsBar.style.width = (results.wins / gamesTotal) * 100 + '%';
  drawsBar.style.width = (results.draws / gamesTotal) * 100 + '%';
  lossesBar.style.width = (results.losses / gamesTotal) * 100 + '%';

  document.getElementById('stripChartWinsSpan').innerText = results.wins;
  document.getElementById('stripChartDrawsSpan').innerText = results.draws;
  document.getElementById('stripChartLossesSpan').innerText = results.losses;

  document.getElementById('stripChartWinsTooltip').innerText = 'Wins: ' + results.wins + '\n (' + ((results.wins / gamesTotal) * 100).toFixed(2) + '%)';
  document.getElementById('stripChartDrawsTooltip').innerText = 'Draws: ' + results.draws + '\n (' + ((results.draws / gamesTotal) * 100).toFixed(2) + '%)';
  document.getElementById('stripChartLossesTooltip').innerText = 'Losses: ' + results.losses + '\n (' + ((results.losses / gamesTotal) * 100).toFixed(2) + '%)';
}

export async function visualizeWorldMaps(pUserGames, pUserID, pOpponentsDataObj, pCountriesPopulation) {
  let countriesObj = await getCountryList(pUserGames, pUserID, pOpponentsDataObj);

  let color1 = {
    red: 255, green: 0, blue: 0,
  };
  let color2 = {
    red: 255, green: 255, blue: 0,
  };
  let color3 = {
    red: 19, green: 233, blue: 19,
  };

  for (let countryCode in countriesObj) {
    countriesObj[countryCode].winning_percentage = calculateWinningPercentage(countriesObj[countryCode].wins, countriesObj[countryCode].draws, countriesObj[countryCode].wins + countriesObj[countryCode].draws + countriesObj[countryCode].losses);
    countriesObj[countryCode].color = (colorGradient(countriesObj[countryCode].winning_percentage, color1, color2, color3));

    if (!countriesObj[countryCode].winning_percentage) {
      countriesObj[countryCode].color = getComputedStyle(document.documentElement).getPropertyValue('--svgMap-noData');
    }
  }

  new svgMap({ // WinLossRatio
    targetElementID: 'svgMapWinLossRatio',
    countryNames: countryNamesEN,
    data: {
      data: {
        wins: {
          name: 'Wins'
        },
        losses: {
          name: 'Losses'
        },
        draws: {
          name: 'Draws'
        },
        winning_percentage: {
          name: 'Winning Percentage'
        }
      },
      applyData: 'winning_percentage',
      values: countriesObj
    },
    colorNoData: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-noData'),
  });

  // opponents per capita

  if (!pCountriesPopulation) {
    document.getElementById('error').innerText = 'Connection Issue';
    document.getElementById('error').style.display = 'block';
    return;
  }

  let countriesOpponents = { ...countryNamesEN };
  for (let countryCode in countriesOpponents) {
    countriesOpponents[countryCode] = { opponents: 0 };
  }

  for (let opponent in pOpponentsDataObj) {
    let countryCodeOfOpponent = pOpponentsDataObj[opponent]?.profile?.country;
    if (!countryCodeOfOpponent || !countriesOpponents[countryCodeOfOpponent]) continue;
    countriesOpponents[countryCodeOfOpponent].opponents++;
  }

  for (let countryCode in countriesOpponents) {
    if (pCountriesPopulation[countryCode] != 0) {
      countriesOpponents[countryCode].opponentsPerCapitaPercent = (countriesOpponents[countryCode].opponents / pCountriesPopulation[countryCode] * 100).toFixed(12);
      countriesOpponents[countryCode].opponentsPer10Mio = (countriesOpponents[countryCode].opponents / pCountriesPopulation[countryCode] * 10_000_000).toFixed(12);
    }
  }

  new svgMap({ // svgMapAmountOpponents
    targetElementID: 'svgMapAmountOpponents',
    countryNames: countryNamesEN,
    data: {
      data: {
        opponentsPerCapitaPercent: {
          name: 'Opponents Per Capita',
          format: '{0} %'
        },
        opponentsPer10Mio: {
          name: 'Opponents Per 10 Mio. Residents',
        },
        opponents: {
          name: 'Opponents'
        },
      },
      applyData: 'opponents',
      values: countriesOpponents
    },
    colorMax: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-colorMax'),
    colorMin: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-colorMin'),
    colorNoData: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-noData'),
  });

  // games per capita

  for (let countryCode in countriesObj) {
    delete countriesObj[countryCode].color;
    if (countriesObj[countryCode].total > 0) {
      countriesObj[countryCode].gamesPerCapitaPercent = (countriesObj[countryCode].total / pCountriesPopulation[countryCode] * 100).toFixed(12);
      countriesObj[countryCode].gamesPer10Mio = (countriesObj[countryCode].total / pCountriesPopulation[countryCode] * 10_000_000).toFixed(12);
    } else {
      countriesObj[countryCode].gamesPerCapitaPercent = 0;
      countriesObj[countryCode].gamesPer10Mio = 0;
    }
  }
  new svgMap({ // svgMapAmountGames
    targetElementID: 'svgMapAmountGames',
    countryNames: countryNamesEN,
    data: {
      data: {
        gamesPerCapitaPercent: {
          name: 'Games Per Capita',
          format: '{0} %'
        },
        gamesPer10Mio: {
          name: 'Games Per 10 Mio. Residents',
        },
        total: {
          name: 'Games'
        },
      },
      applyData: 'total',
      values: countriesObj
    },
    colorMax: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-colorMax'),
    colorMin: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-colorMin'),
    colorNoData: getComputedStyle(document.documentElement).getPropertyValue('--svgMap-noData'),
  });
}

function calculateWinningPercentage(wins, draws, games) {
  if (!games) {
    return 0;
  }

  const percentage = (wins + 0.5 * draws) / games;
  return percentage.toFixed(2); // Limiting to 2 decimal places
}

function colorGradient(fadeFraction, rgbColor1, rgbColor2, rgbColor3) {
  var color1 = rgbColor1;
  var color2 = rgbColor2;
  var fade = fadeFraction;

  // Do we have 3 colors for the gradient? Need to adjust the params.
  if (rgbColor3) {
    fade = fade * 2;

    // Find which interval to use and adjust the fade percentage
    if (fade >= 1) {
      fade -= 1;
      color1 = rgbColor2;
      color2 = rgbColor3;
    }
  }

  var diffRed = color2.red - color1.red;
  var diffGreen = color2.green - color1.green;
  var diffBlue = color2.blue - color1.blue;

  var gradient = {
    red: parseInt(Math.floor(color1.red + (diffRed * fade)), 10),
    green: parseInt(Math.floor(color1.green + (diffGreen * fade)), 10),
    blue: parseInt(Math.floor(color1.blue + (diffBlue * fade)), 10),
  };
  return 'rgb(' + gradient.red + ',' + gradient.green + ',' + gradient.blue + ')';
}

async function getCountryList(pUserGames, pUserID, pOpponentsDataObj) {
  let countriesObj = { ...countryNamesEN };
  for (let countryCode in countriesObj) {
    countriesObj[countryCode] = { wins: 0, draws: 0, losses: 0, total: 0 };
  }

  // sort games to countries
  for (let game of pUserGames) {
    if (game.players.black.aiLevel || game.players.white.aiLevel) continue; // if opponent is stockfish

    if (game.players.white?.user?.id == pUserID && game.players.black?.user?.id) { // if opponent has black pieces
      if (!pOpponentsDataObj[game.players.black.user.id]?.profile?.country) continue;

      if (pOpponentsDataObj[game.players.black.user.id].profile.country == '_belarus-wrw') { pOpponentsDataObj[game.players.black.user.id].profile.country = 'BY'; }
      if (pOpponentsDataObj[game.players.black.user.id].profile.country == '_east-turkestan') { pOpponentsDataObj[game.players.black.user.id].profile.country = 'CN'; }
      if (pOpponentsDataObj[game.players.black.user.id].profile.country == '_russia-wbw') { pOpponentsDataObj[game.players.black.user.id].profile.country = 'RU'; }

      if (!countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)]) continue;
      if (game?.winner == 'white') { // win
        countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)].wins++;
        countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)].total++;
      }
      if (game?.winner == 'black') { // loss
        countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)].losses++;
        countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)].total++;
      }
      if (game?.status == 'draw') { // draw
        countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)].draws++;
        countriesObj[(pOpponentsDataObj[game.players.black.user.id].profile.country)].total++;
      }

    } else if (game.players.black?.user?.id == pUserID && game.players.white?.user?.id) { // if opponent has white pieces
      if (!pOpponentsDataObj[game.players.white.user.id]?.profile?.country) continue;

      if (pOpponentsDataObj[game.players.white.user.id].profile.country == '_belarus-wrw') { pOpponentsDataObj[game.players.white.user.id].profile.country = 'BY'; }
      if (pOpponentsDataObj[game.players.white.user.id].profile.country == '_east-turkestan') { pOpponentsDataObj[game.players.white.user.id].profile.country = 'CN'; }
      if (pOpponentsDataObj[game.players.white.user.id].profile.country == '_russia-wbw') { pOpponentsDataObj[game.players.white.user.id].profile.country = 'RU'; }

      if (!countriesObj[(pOpponentsDataObj[game.players.white.user.id].profile.country)]) continue;
      if (game?.winner == 'white') { // loss
        countriesObj[(pOpponentsDataObj[game.players.white.user.id].profile.country)].losses++;
        countriesObj[(pOpponentsDataObj[game.players.white.user.id].profile.country)].total++;
      }
      if (game?.winner == 'black') { // win
        countriesObj[(pOpponentsDataObj[game.players.white.user.id].profile.country)].total++;
      }
      if (game?.status == 'draw') { // draw
        countriesObj[(pOpponentsDataObj[game.players.white.user.id].profile.country)].total++;
      }
    }
  }

  return countriesObj;
}

export function visualizePieceMoves(pUserGames, pUserID) {

  let piecesMoves = {
    King: 0,
    Queen: 0,
    Rook: 0,
    Bishop: 0,
    Knight: 0,
    Pawn: 0,
  }
  let totalMoves = 0;

  for (let game of pUserGames) {
    let evenMoves;
    if (game.players?.white?.user?.id == pUserID) {
      evenMoves = 0;
    } else {
      evenMoves = 1;
    }
    // filter only white/black moves
    let moves = game.moves.split(' ').filter((_, index) => index % 2 === evenMoves);
    for (let move of moves) {
      switch (move[0]) {
        case 'K':
          piecesMoves.King++;
          break;
        case '0': // Castling
          piecesMoves.King++;
          break;
        case 'Q':
          piecesMoves.Queen++;
          break;
        case 'R':
          piecesMoves.Rook++;
          break;
        case 'B':
          piecesMoves.Bishop++;
          break;
        case 'N':
          piecesMoves.Knight++;
          break;
        default:
          piecesMoves.Pawn++;
          break;
      }
      totalMoves++;
    }
  }

  if (totalMoves == 0) { return; }

  new Chart(document.getElementById('piecesMovesChart'), {
    type: "pie",
    data: {
      labels: Object.keys(piecesMoves),
      datasets: [{
        data: Object.values(piecesMoves),
        backgroundColor: [
          getComputedStyle(document.documentElement).getPropertyValue('--pieChart-color1'),
          getComputedStyle(document.documentElement).getPropertyValue('--pieChart-color2'),
          getComputedStyle(document.documentElement).getPropertyValue('--pieChart-color3'),
          getComputedStyle(document.documentElement).getPropertyValue('--pieChart-color4'),
          getComputedStyle(document.documentElement).getPropertyValue('--pieChart-color5'),
          getComputedStyle(document.documentElement).getPropertyValue('--pieChart-color6'),
        ],
      }]
    },
    options: {
      responsive: false,
      plugins: {
        tooltip: {
          backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-background'),
          titleColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-black'),
          bodyColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-gray'),
          callbacks: {
            label: (context) => {
              return `${(context.parsed / totalMoves * 100).toFixed(2)}% (${context.parsed})`;
            }
          }
        }
      }
    }
  });
}

export function visualizeResultsByOpponentsRating(pUserGames, pUserID) {

  let data = {};
  for (let i = 400; i <= 3400; i += 100) {
    data[i] = { wins: 0, draws: 0, losses: 0 };
  }

  for (let game of pUserGames) {
    if (game.players.black.aiLevel || game.players.white.aiLevel) continue; // if opponent is stockfish

    let opponentRatingRoundedOff;
    if (game.players.white?.user?.id == pUserID) {
      if (game.players.black?.rating) { // if opponent is anonymous
        opponentRatingRoundedOff = Math.floor(game.players.black.rating / 100) * 100;
        if (opponentRatingRoundedOff < 400 || opponentRatingRoundedOff > 3400) continue;
        if (game?.winner == 'white') {
          data[opponentRatingRoundedOff].wins++;
        } else if (game?.winner == 'black') {
          data[opponentRatingRoundedOff].losses++;
        } else if (game?.status == 'draw') {
          data[opponentRatingRoundedOff].draws++;
        }
      }

    } else if (game.players.black?.user?.id == pUserID) {
      if (game.players.white?.rating) { // if opponent is anonymous
        opponentRatingRoundedOff = Math.floor(game.players.white.rating / 100) * 100;
        if (opponentRatingRoundedOff < 400 || opponentRatingRoundedOff > 3400) continue;
        if (game?.winner == 'white') {
          data[opponentRatingRoundedOff].losses++;
        } else if (game?.winner == 'black') {
          data[opponentRatingRoundedOff].wins++;
        } else if (game?.status == 'draw') {
          data[opponentRatingRoundedOff].draws++;
        }
      }
    }
  }
  // Get values in percentage
  for (let rating in data) {
    data[rating].total = data[rating].wins + data[rating].losses + data[rating].draws;

    data[rating].winsPercent = data[rating].total != 0 ? data[rating].wins / data[rating].total : 0;
    data[rating].drawsPercent = data[rating].total != 0 ? data[rating].draws / data[rating].total : 0;
    data[rating].lossesPercent = data[rating].total != 0 ? data[rating].losses / data[rating].total : 0;
  }

  let labelWin = 'Wins', labelDraw = 'Draws', labelLoss = 'Losses';
  let winValues = Object.values(data).map(item => item.wins);
  let drawValues = Object.values(data).map(item => item.draws);
  let lossValues = Object.values(data).map(item => item.losses);

  new Chart(document.getElementById('resultsRatingChart'), {
    type: 'bar',
    options: {
      plugins: {
        legend: {
          display: true
        },
        tooltip: {
          backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-background'),
          titleColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-black'),
          bodyColor: getComputedStyle(document.documentElement).getPropertyValue('--tooltip-gray'),

          mode: 'index',
          itemSort: (a, b) => { b.datasetIndex - a.datasetIndex; },
          callbacks: {
            label: (context) => { // todo: reverse order
              let label = context.dataset.label || '';

              let wins = winValues[context.dataIndex];
              let draws = drawValues[context.dataIndex];
              let losses = lossValues[context.dataIndex];
              let absoluteValue;
              if (label == labelWin) {
                absoluteValue = wins;
              } else if (label == labelDraw) {
                absoluteValue = draws;
              } else if (label == labelLoss) {
                absoluteValue = losses;
              }
              return `${label}: ${(context.parsed.y * 100).toFixed(2)}% (${absoluteValue})`;
            }
          }
        }
      },
      scales: {
        x: {
          stacked: true,
        },
        y: {
          stacked: true
        }
      },
    },
    data: {
      labels: Object.keys(data),
      datasets: [{
        label: labelLoss,
        data: Object.values(data).map(item => item.lossesPercent),
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-loss-color'),
        borderColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-loss-border-color'),
        borderWidth: '1',
        grouped: true,
      }, {
        label: labelDraw,
        data: Object.values(data).map(item => item.drawsPercent),
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-draw-color'),
        borderColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-draw-border-color'),
        borderWidth: '1',
        grouped: true,
      }, {
        label: labelWin,
        data: Object.values(data).map(item => item.winsPercent),
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-win-color'),
        borderColor: getComputedStyle(document.documentElement).getPropertyValue('--chart-win-border-color'),
        borderWidth: '1',
        grouped: true,
      }]
    }
  });
}