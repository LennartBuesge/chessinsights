# Chess Insights

This project shows interesting and useful statistics to every lichess-player who likes to improve on chess. It is available at https://www.chessinsights.de/.



## Local Installation

Running this project locally requires downloading all files and copying them to your web server's document root. You may use Apache, Nginx, or any other web server of your choice.
## Screenshot

Here is an example screenshot of our dashboard showing all rated bullet games by GM Alireza Firouzja ([alireza2003](https://lichess.org/@/alireza2003)):

![rated bullet games by GM Alireza Firouzja](https://chessinsights.de/screenshots/alireza2003%20light.png)


## Attribution

This project uses the following libraries:

- [svgMap](https://github.com/StephanWagner/svgMap)
- [Chart.js](https://www.chartjs.org/)

And the following APIs:

- [Lichess API](https://lichess.org/api)
- [REST Countries](https://restcountries.com/)

Special thanks to [lichess](https://lichess.org/) for providing the data.
